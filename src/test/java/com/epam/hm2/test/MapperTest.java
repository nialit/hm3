package com.epam.hm2.test;

import com.epam.hm2.customwriteable.IntermediateStatisticHolder;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Test;

import java.io.IOException;

public class MapperTest {

    private final String
        testLine =
        "ip136 - - [24/Apr/2011:12:22:37 -0400] \"GET /sun_ss5/ss5-newfan.jpg HTTP/1.1\" 200 74139 \"http://host2/sun_ss5/\" \"Mozilla/5.0 (Windows; U; Windows NT 5.1; sv-SE; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 ( .NET CLR 3.5.30729)\"\n";

    @Test
    public void testMap() throws IOException, InterruptedException {
        Text value = new Text(testLine);
        new MapDriver<LongWritable, Text, Text, Text>()
            .withMapper(new com.epam.hm2.map.RequestBytesMapper())
            .withInput(new LongWritable(0), new Text(value))
            .withOutput(new Text("ip136"), new Text("1,74139"))
            .runTest();
    }

    @Test
    public void testMapWithCustomWriteable() throws IOException, InterruptedException {
        Text value = new Text(testLine);
        new MapDriver<LongWritable, Text, Text, IntermediateStatisticHolder>()
            .withMapper(new com.epam.hm2.customwriteable.map.RequestBytesMapper())
            .withInput(new LongWritable(0), new Text(value))
            .withOutput(new Text("ip136"), new IntermediateStatisticHolder(1, 74139))
            .runTest();
    }
}
