package com.epam.hm2.test;

import com.epam.hm2.customwriteable.IntermediateStatisticHolder;
import com.epam.hm2.customwriteable.reduce.FinalStatisticHolder;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;

public class ReducerTest {


    @Test
    public void reduceTest() throws IOException,
                                    InterruptedException {
        new ReduceDriver<Text, Text, Text, NullWritable>()
            .withReducer(new com.epam.hm2.reduce.RequestBytesReducer())
            .withInput(new Text("ip136"),
                       Arrays.asList(new Text("2,100"), new Text("1,200")))
            .withOutput(new Text("ip136,100,300"), NullWritable.get())
            .runTest();
    }

    @Test
    public void reduceWithCustomWritableOutputTest() throws IOException,
                                                            InterruptedException {
        new ReduceDriver<Text, IntermediateStatisticHolder, Text, FinalStatisticHolder>()
            .withReducer(new com.epam.hm2.customwriteable.reduce.RequestBytesReducer())
            .withInput(new Text("ip136"),
                       Arrays.asList(new IntermediateStatisticHolder(2, 100), new IntermediateStatisticHolder(1, 200)))
            .withOutput(new Text("ip136"), new FinalStatisticHolder(100, 300))
            .runTest();
    }
}
