package com.epam.hm2;

import com.epam.hm2.counter.Browser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogLineInfo {

    public final String ip;
    public final long totalBytes;
    public final Browser browser;

    public LogLineInfo(String ip, long totalBytes, long entries, Browser browser) {
        this.ip = ip;
        this.totalBytes = totalBytes;
        this.browser = browser;
    }

    public String toString() {
        return String.format("%s,%d", ip, totalBytes);
    }


    private static final String logLineRegex = "(\\w+) - - (\\[.*\\]) \"(.*?)\" (\\d+) (\\d+|-) \"(.*?)\" \"(.*)";
    private static final Pattern regexpGrouper = Pattern.compile(logLineRegex);

    public static LogLineInfo processLine(String line) {
        Matcher m = regexpGrouper.matcher(line);
        return m.find() ? new LogLineInfo(m.group(1),
                                          m.group(5).equals("-") ? 0 : Long.valueOf(m.group(5)),
                                          1,
                                          getBrowser(m.group(7))) : null;
    }

    private static Browser getBrowser(String val) {
        val = val.toLowerCase();
        if (val.contains("firefox")) {
            return Browser.FIREFOX;
        } else if (val.contains("msie")) {
            return Browser.IE;
        } else {
            return Browser.OTHER;
        }
    }
}
