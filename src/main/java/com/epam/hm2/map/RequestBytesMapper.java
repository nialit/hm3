package com.epam.hm2.map;

import com.epam.hm2.LogLineInfo;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class RequestBytesMapper extends Mapper<LongWritable, Text, Text, Text> {

    private final String valueFormat = "%d,%d";

    protected void map(LongWritable key, Text value,
                       Context context) throws IOException, InterruptedException {
        LogLineInfo lineInfo = LogLineInfo.processLine(value.toString());
        if (lineInfo == null) {
            return;
        }
        context.getCounter(lineInfo.browser).increment(1);
        context.write(new Text(lineInfo.ip), new Text(String.format(valueFormat, 1, lineInfo.totalBytes)));
    }

}

