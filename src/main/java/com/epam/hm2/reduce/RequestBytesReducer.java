package com.epam.hm2.reduce;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RequestBytesReducer extends Reducer<Text, Text, Text, NullWritable> {


    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context
    ) throws IOException, InterruptedException {
        long amt = 0;
        long totalBytes = 0;
        for (Text val : values) {
            String[] lineVals = val.toString().split(",");
            if (Long.valueOf(lineVals[0]) == 0) {
                System.out.println(key + ":" + val);
            }
            amt += Long.valueOf(lineVals[0]);
            totalBytes += Long.valueOf(lineVals[1]);
        }
        context.write(new Text(String.format("%s,%d,%d", key, totalBytes / amt, totalBytes)), NullWritable.get());
    }
}

