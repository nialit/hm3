package com.epam.hm2.reduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RequestBytesCombiner extends Reducer<Text, Text, Text, Text> {


    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context
    ) throws IOException, InterruptedException {
        long entries = 0;
        long totalBytes = 0;
        for (Text val : values) {
            String[] lineVals = val.toString().split(",");
            entries += Long.valueOf(lineVals[0]);
            totalBytes += Long.valueOf(lineVals[1]);
        }
        context.write(key, new Text(String.format("%d,%d", entries, totalBytes)));
    }
}
