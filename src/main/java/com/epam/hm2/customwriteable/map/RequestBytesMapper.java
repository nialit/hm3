package com.epam.hm2.customwriteable.map;

import com.epam.hm2.LogLineInfo;
import com.epam.hm2.customwriteable.IntermediateStatisticHolder;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class RequestBytesMapper extends Mapper<LongWritable, Text, Text, IntermediateStatisticHolder> {

    protected void map(LongWritable key, Text value,
                       Context context) throws IOException, InterruptedException {
        LogLineInfo lineInfo = LogLineInfo.processLine(value.toString());
        if (lineInfo == null) {
            System.out.println(value.toString());
            return;
        }
        context.getCounter(lineInfo.browser).increment(1);
        context.write(new Text(lineInfo.ip), new IntermediateStatisticHolder(1, lineInfo.totalBytes));
    }

}

