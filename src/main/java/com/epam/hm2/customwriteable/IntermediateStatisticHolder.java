package com.epam.hm2.customwriteable;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class IntermediateStatisticHolder implements Writable {

    private LongWritable entries;
    private LongWritable totalBytes;

    @Override
    public String toString() {
        return String.format("%d,%d", entries.get(), totalBytes.get());
    }

    public IntermediateStatisticHolder() {
        this.set(new LongWritable(), new LongWritable());
    }

    public IntermediateStatisticHolder(LongWritable entries, LongWritable totalBytes) {
        this.set(entries, totalBytes);
    }

    public IntermediateStatisticHolder(long entries, long totalBytes) {
        this.set(new LongWritable(entries), new LongWritable(totalBytes));
    }

    public LongWritable getEntries() {
        return entries;
    }

    public LongWritable getTotalBytes() {
        return totalBytes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IntermediateStatisticHolder that = (IntermediateStatisticHolder) o;

        if (entries != null ? !entries.equals(that.entries) : that.entries != null) {
            return false;
        }
        return totalBytes != null ? totalBytes.equals(that.totalBytes) : that.totalBytes == null;

    }

    @Override
    public int hashCode() {
        int result = entries != null ? entries.hashCode() : 0;
        result = 31 * result + (totalBytes != null ? totalBytes.hashCode() : 0);
        return result;
    }

    public void set(LongWritable entries, LongWritable totalBytes) {
        this.entries = entries;
        this.totalBytes = totalBytes;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        entries.write(out);
        totalBytes.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        entries.readFields(in);
        totalBytes.readFields(in);
    }
}
