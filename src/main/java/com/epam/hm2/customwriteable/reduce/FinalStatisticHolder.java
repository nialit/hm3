package com.epam.hm2.customwriteable.reduce;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class FinalStatisticHolder implements Writable {

    private LongWritable average;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FinalStatisticHolder that = (FinalStatisticHolder) o;

        if (average != null ? !average.equals(that.average) : that.average != null) {
            return false;
        }
        return totalBytes != null ? totalBytes.equals(that.totalBytes) : that.totalBytes == null;

    }

    @Override
    public int hashCode() {
        int result = average != null ? average.hashCode() : 0;
        result = 31 * result + (totalBytes != null ? totalBytes.hashCode() : 0);
        return result;
    }

    private LongWritable totalBytes;

    @Override
    public String toString() {
        return String.format("%d,%d", average.get(), totalBytes.get());
    }

    public FinalStatisticHolder() {
        this.set(new LongWritable(), new LongWritable());
    }

    public FinalStatisticHolder(LongWritable average, LongWritable totalBytes) {
        this.set(average, totalBytes);
    }

    public FinalStatisticHolder(long average, long totalBytes) {
        this.set(new LongWritable(average), new LongWritable(totalBytes));
    }

    public LongWritable getAverage() {
        return average;
    }

    public LongWritable getTotalBytes() {
        return totalBytes;
    }

    public void set(LongWritable entries, LongWritable totalBytes) {
        this.average = entries;
        this.totalBytes = totalBytes;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        average.write(out);
        totalBytes.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        average.readFields(in);
        totalBytes.readFields(in);
    }
}
