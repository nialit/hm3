package com.epam.hm2.customwriteable.reduce;

import com.epam.hm2.customwriteable.IntermediateStatisticHolder;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RequestBytesCombiner extends Reducer<Text, IntermediateStatisticHolder, Text, IntermediateStatisticHolder> {


    @Override
    protected void reduce(Text key, Iterable<IntermediateStatisticHolder> values, Context context
    ) throws IOException, InterruptedException {
        long entries = 0;
        long totalBytes = 0;
        for (IntermediateStatisticHolder val : values) {
            entries += val.getEntries().get();
            totalBytes += val.getTotalBytes().get();
        }
        context.write(key, new IntermediateStatisticHolder(entries, totalBytes));
    }
}

