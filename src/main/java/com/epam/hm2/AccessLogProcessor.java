package com.epam.hm2;

import com.epam.hm2.customwriteable.IntermediateStatisticHolder;
import com.epam.hm2.customwriteable.reduce.FinalStatisticHolder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.util.Iterator;


public class AccessLogProcessor extends Configured implements Tool {

    boolean useCustomWriteable = true;

    @Override
    public int run(String[] args) throws Exception {
        Class<? extends Mapper> mapperClass =
            useCustomWriteable ? com.epam.hm2.customwriteable.map.RequestBytesMapper.class
                               : com.epam.hm2.map.RequestBytesMapper.class;
        Class<? extends Reducer> reducerClass =
            useCustomWriteable ? com.epam.hm2.customwriteable.reduce.RequestBytesReducer.class
                               : com.epam.hm2.reduce.RequestBytesReducer.class;
        Class<? extends Reducer> combinerClass =
            useCustomWriteable ? com.epam.hm2.customwriteable.reduce.RequestBytesCombiner.class
                               : com.epam.hm2.reduce.RequestBytesCombiner.class;
        Class<? extends Writable> mapOutputValueClass =
            useCustomWriteable ? IntermediateStatisticHolder.class : Text.class;
        Class<? extends Writable> outputValueClass =
            useCustomWriteable ? FinalStatisticHolder.class : NullWritable.class;
        Configuration conf = getConf();
        Job job = Job.getInstance(conf, "log's statistic counter");
        job.setJarByClass(getClass());
        job.setCombinerClass(combinerClass);
        job.setMapperClass(mapperClass);
        job.setReducerClass(reducerClass);
        job.setNumReduceTasks(1);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(mapOutputValueClass);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(outputValueClass);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
        SequenceFileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        int result = job.waitForCompletion(false) ? 1 : 0;
        for (CounterGroup cg : job.getCounters()) {
            StringBuilder sb = new StringBuilder();
            Iterator<Counter> counterIter = cg.iterator();
            while (counterIter.hasNext()) {
                Counter ctr = counterIter.next();
                sb.append(String.format("%s:%s\n", ctr.getName(), ctr.getValue()));
            }
            System.out.println(cg.getName() + ":\n" + sb.toString());
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();
        int exitCode = ToolRunner.run(new AccessLogProcessor(), args);
        System.out.println("Execution time:" + String.valueOf(System.currentTimeMillis() - start));
        System.exit(exitCode);
    }
}
